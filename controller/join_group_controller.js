const Group = require("../model/group");
const Member = require("../model/member");

const render = async (req, res) => {
  await Group.open();

  const allGroups = await Group.getAllGroups();

  res.render("join_group", {
    title: "Join group",
    groups: allGroups,
  });

  await Group.close();
};

const joinGroup = async (req, res) => {
  await Member.open();

  const { id, error } = await Member.joinGroup(
    req.body.groupId,
    req.body.firstName,
    req.body.lastName,
    req.body.education
  );

  res.json({ id, error });

  await Member.close();
};

module.exports = {
  render,
  joinGroup,
};
