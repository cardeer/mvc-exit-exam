const render = async (req, res) => {
  res.render("index", {
    title: "Home",
  });
};

module.exports = {
  render,
};
