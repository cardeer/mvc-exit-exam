const Group = require("../model/group");

const render = async (req, res) => {
  await Group.open();

  const allGroups = await Group.getAllGroups();

  res.render("simulation", {
    title: "Simulation",
    groups: allGroups,
  });

  await Group.close();
};

module.exports = {
  render,
};
