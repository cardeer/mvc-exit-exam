const Group = require("../model/group");

const render = async (req, res) => {
  res.render("create_group", {
    title: "Create group",
  });
};

const createGroup = async (req, res) => {
  await Group.open();

  const { id, error } = await Group.createGroup(
    req.body.groupName,
    req.body.firstName,
    req.body.lastName,
    req.body.education
  );

  res.json({
    id: id,
    error: false,
  });

  await Group.close();
};

module.exports = {
  render,
  createGroup,
};
