require("dotenv").config();

const express = require("express");
const app = express();
const path = require("path");
const port = 8888;

const engine = require("ejs-locals");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, "assets"))); // serve static files (assets)

app.set("views", path.join(__dirname, "view")); // set view files path
app.engine("ejs", engine); // set ejs engine (use more features)
app.set("view engine", "ejs"); // set view engine

require("./routes")(app); // all routes

// start server
app.listen(port);
