const DB = require("../db");

class Member extends DB {
  constructor() {
    super();
  }

  static async joinGroup(groupId, firstName, lastName, education) {
    const {
      rows,
    } = await this.query(
      "SELECT COUNT(*) as count FROM members WHERE group_id = ?",
      [groupId]
    );

    if (rows[0].count < 11) {
      await this.query(
        "INSERT INTO members (first_name, last_name, education, group_id) VALUES (?, ?, ?, ?)",
        [firstName, lastName, education, groupId]
      );
      return { id: `E${groupId}${rows[0].count + 1}`, error: false };
    } else {
      return { id: null, error: "Group limit 11 members" };
    }
  }
}

module.exports = Member;
