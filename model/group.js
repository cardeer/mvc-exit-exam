const DB = require("../db");

class Group extends DB {
  constructor() {
    super();
  }

  static async getAllGroups() {
    const { rows } = await this.query("SELECT * FROM groups");
    return rows;
  }

  static async getGroup(groupId) {
    const { rows } = await this.query("SELECT * FROM groups WHERE id = ?", [
      groupId,
    ]);
    return rows;
  }

  static async getGropMembers(groupId) {
    const {
      rows,
    } = await this.query("SELECT * FROM members WHERE group_id = ?", [groupId]);
    return rows;
  }

  static async createGroup(groupName, firstName, lastName, education) {
    const { rows } = await this.query("INSERT INTO groups (name) VALUES (?)", [
      groupName,
    ]);

    await this.query(
      "INSERT INTO members (first_name, last_name, education, group_id) VALUES (?, ?, ?, ?)",
      [firstName, lastName, education, rows.insertId]
    );

    return { id: `E${rows.insertId}1`, error: false };
  }
}

module.exports = Group;
