// controller files
const indexController = require("./controller/index_controller");
const createGroupController = require("./controller/create_group_controller");
const joinGroupController = require("./controller/join_group_controller");
const simulationController = require("./controller/simulation_controller");

// routes
module.exports = (app) => {
  app.get("/", indexController.render);
  app.get("/create_group", createGroupController.render);
  app.get("/join_group", joinGroupController.render);
  app.get("/simulation", simulationController.render);

  app.post("/create_group/create", createGroupController.createGroup);
  app.post("/join_group/join", joinGroupController.joinGroup);
};
