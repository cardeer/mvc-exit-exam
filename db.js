const mysql = require("mysql2/promise");

class DB {
  constructor() {
    this.db = null;
  }

  static async open() {
    this.db = await mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
    });
  }

  static async close() {
    await this.db.end();
  }

  static async query(queryString, values = []) {
    try {
      const [rows, fields] = await this.db.query(queryString, values);
      return { rows, fields, error: false };
    } catch (error) {
      console.error(error);
      return { error: error };
    }
  }
}

module.exports = DB;
